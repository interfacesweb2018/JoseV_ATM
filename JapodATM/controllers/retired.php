<?php 

	if (!isset($_SESSION['permission'])) {
		echo "<script>location.href='?action=login';</script>";
	}

	if(isset($_POST['siguiente'])){
		echo "<script>location.href='?action=home';</script>";
	}

	class Retiro extends User_Session{
        
   
        private $retired_cash;
        private $available_cash;
        private $cash_actu;
        
		

		public function __construct(){
                $this->available_cash = $_SESSION['cash'];
            
		}


		public function mostrarActual(){
			return $this->cash_actu;
        }
        
        public function retired($retired_cash){
			$this->retired_cash = intval($retired_cash);
			$this->cash_actu = $this->available_cash - $this->retired_cash;
			
			if($this->cash_actu > 0){
				return true;
			}
			
		}
	}

	$retiro = new Retiro();

	if (isset($_POST['retiro'])){
		if($retiro->retired($_POST['retired_cash'])){
			$ok = true;
		}else{
			echo "<script>location.href='?action=login';</script>";
			$session->destroy();
		}
	}

	require('views/retired.php');
?>


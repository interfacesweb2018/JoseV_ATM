<?php 

        require('views/header.php');
        require('models/users.php');
        require('controllers/user_session.php');
       
        if (session_status() !== PHP_SESSION_ACTIVE) { $session = new User_Session(); $session->open(); }


        if (empty($_GET["action"])) {
              $action = "home"; 
          } else {
              $action = htmlentities($_GET["action"]);
          }


?>

<body style="background: gray">
    
<?php




    if (is_file("controllers/".$action.".php")) {
        require("controllers/".$action.".php");
    } else {

        require("controllers/home.php");
    }
?>
</body>


